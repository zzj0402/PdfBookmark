# -*- coding: utf-8 -*-
import unittest
from PdfBookmark import PdfBookmark


class TestBookmark(unittest.TestCase):

    def test_export(self):
        bm1 = PdfBookmark('Samples/a1.pdf')
        bm1.exportBookmark('Samples/a1.bm')

    def test_zoom(self):
        bm1 = PdfBookmark(
            'test/transfer_learning_for_detecting_unknown_network_attacks.pdf')
        bm1.exportBookmark(
            'test/transfer_learning_for_detecting_unknown_network_attacks.bm')


if __name__ == '__main__':
    unittest.main()
